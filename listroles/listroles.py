# listroles cog by HarJIT written for appu1232's selfbot.
# may be used under same terms as appu1232's selfbot itself (MIT)

import discord, cogs.utils.checks, discord.ext.commands

import getopt, io

def describe(i):
    r = ""
    if i.mentionable:
        r += ", mentionable"
    if i.hoist:
        r += ", hoisted"
    if i.color.value:
        r += ", #%06x" % i.color.value
    else:
        r += ", colourless"
    #r += ", pos-%02d" % i.position
    if i.managed:
        r += ", managed"
    return r.lstrip(" ,")

def pad(s):
    if len(s) < 20:
        s += "\x20" * (20 - len(s))
    return s

async def softsend(mybot, ctx, f, prefix="", suffix=""):
    r = ""
    f.seek(0)
    lines = f.readlines()
    limit = 2000 - len(suffix)
    while lines:
        #print(lines)
        cut = limit - len(r)
        line = lines.pop(0)
        if (len(line) > limit) and (cut > 0):
            # Because pushing on front, have to push in reverse order.
            lines.insert(0, line[cut:])
            lines.insert(0, line[:cut])
        elif len(r + line) > limit:
            lines.insert(0, prefix + line)
            await mybot.send_message(ctx.message.channel, r + suffix)
            r = ""
        else:
            r += line
    if r:
        await mybot.send_message(ctx.message.channel, r)

class Listroles(object):
    def __init__(self, mybot):
        self.mybot = mybot
    @discord.ext.commands.group(pass_context = True)
    async def listroles(self, ctx):
        """List roles on a server by number of members.  Accepts --all, --color, --min=(number)."""
        argv = ctx.message.content[cogs.utils.checks.cmd_prefix_len():]
        argv = argv.strip().split()
        opts, args = getopt.gnu_getopt(argv[1:], "", ["colour", "color", "all", "min="])
        opts = dict(opts)
        minimum = int(opts["--min"]) if "--min" in opts else 2
        #
        counts = {}
        roles = list(ctx.message.server.roles)[:]
        for i in roles:
            counts[i.id] = 0
        for i in ctx.message.server.members:
            for j in i.roles:
                counts[j.id] += 1
        roles.sort(key = lambda i: -counts[i.id])
        #
        out = io.StringIO()
        out.write(self.mybot.bot_prefix+"```\n")
        for i in roles:
            if "--all" not in opts:
                if ("--color" in opts or "--colour" in opts) and not i.color.value:
                    continue
                elif counts[i.id] < minimum:
                    continue
            print(pad("%s: %d" % (i.name, counts[i.id])) + "(" + describe(i) + ")", file=out)
        out.write("\n```")
        await softsend(self.mybot, ctx, out, "```\n", "\n```")

def setup(mybot):
    mybot.add_cog(Listroles(mybot))